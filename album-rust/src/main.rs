extern crate rand;

use rand::Rng;

use std::collections::HashSet;

pub struct Album {
    estado:  HashSet<i32>
}

pub struct Sobre {
    figuritas: Vec<i32>
}

pub fn crear_album() -> Album {
    let res = Album {
        estado: HashSet::new()
    };
    return res;
}

pub fn crear_sobre() -> Sobre {
    Sobre {
        figuritas: (0..5).map(|_| rand::thread_rng().gen_range(1, 501)).collect()
    }
}

pub trait Albumoso {
    fn agregar_sobre(&mut self, sobre: Sobre);
    fn pegar(&mut self, figurita: i32);
    fn esta_lleno(&self) -> bool;
}

pub trait Sobresoso {
    fn abrir(&self) -> &Vec<i32>;
}

impl Albumoso for Album {
    fn agregar_sobre(&mut self, sobre: Sobre) {
        for figu in sobre.abrir() {
            self.pegar(*figu);
        }
    }

    fn pegar(&mut self, figurita: i32) {
        self.estado.insert(figurita);
    }

    fn esta_lleno(&self) -> bool {
        return self.estado.len() >= 500;
    }
}

impl Sobresoso for Sobre {
    fn abrir(&self) -> &Vec<i32> {
        return &self.figuritas;
    }
}

fn completar_album() -> u32 {
    let mut album = crear_album();
    let mut cantidad: u32 = 0;
    while !album.esta_lleno() {
        album.agregar_sobre(crear_sobre());
        cantidad += 1;
    }
    return cantidad;
}

fn main() {
    let mut total: u32 = 0;
    for _ in 1..1000 {
        total += completar_album();
    }
    println!("Listorti!: {}", total);
}
