from album import Album
from sobreFigus import SobreFigus

class Simulacion:
    def correr(self):
        res = 0
        album = Album()

        while not album.estaLleno():
            album.agregarSobre(SobreFigus())
            res += 1

        return res
