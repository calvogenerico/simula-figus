class Album:
    
    def __init__(self):
        self.estado = set()

    # Agrega todas las cartas de un sobre. Asume que el sobre tiene cartas correctas
    def agregarSobre(self, sobre):
        figuritas = sobre.abrir()
        for figurita in figuritas:
            self.pegar(figurita)

    # Pega una figurita. Si la figurita estaba ya pegada no hace nada.
    def pegar(self, figurita):
        self.estado.add(figurita)


    def estaLleno(self):
        return len(self.estado) >= 500
